const { execFile, spawn } = require('child_process');

function S_CONNECTION({ ssid, iface, status, info = {} }){
	this.iface = iface;
	this.status = status;
	this.ssid = ssid;
	this.ip = info.ip;
	this.mask = info.mask;
}

function S_IP(arr){
	const split = arr[2]?.split('\/') ?? [];

	this.iface = arr[0];
	this.status = arr[1];
	this.ip = split[0];
	this.mask = Number(split[1]);
}

function S_SSID(arr){
	this.bssid = arr[0].replace(/\\:/g, ':');
	this.ssid = arr[1];
	this.signal = Number(arr[2]);
	this.active = Boolean(arr[3]?.match(/\*/g));
	this.security = arr[4]?.trim().split(/[\s]+/) ?? [];
}

class _ARP_SCAN_HELPER {
	flags = {
		inProcess: false,
	};

	constructor(){
		const self = this;
	}

	_returns({ error, data }){
		const self = this;

		if(error) return { status: false, error, data };

		return { status: true, data };
	}

	_execute(cmdline, options = {}){
		const self = this;

		return new Promise(async (resolve, reject) => {
			if(self.flags.inProcess) {
				resolve({ error: 'Another process is running' });
			}

			// if a command is a non-blocking command, skip setting flag
			self.flags.inProcess = !options.nonBlock;

			const res = { output: '', error: '' };

			const args = cmdline.trim().match(/(["'].*?["']|[^"'\s]+)+(?=\s*|\s*$)/g);
			const command = args.shift();
			const _options = {
					timeout: options.timeout ?? 10_000,
					maxBuffer: 10_000 * 1024,
					shell: options.shell,
				};

			const shell = spawn(
					command,
					args.map(arg => arg.replace(/^['"](.*)['"]$/, '$1')),
					_options
				);

			shell.stdout.on('data', rec => { res.output += rec; });
			shell.stderr.on('data', rec => { res.error += rec; });
			shell.on('error', rec => { res.error += rec; });
			shell.on('close', code => {
					res.output = res.output.trim();
					res.error = res.error.trim();

					resolve(res);

					self.flags.inProcess = false;
				});
		});
	}

	async scan(iface, ip, mask){
		const self = this;

		if(!mask){
			const maskRegExp = ip.match(/\/([0-9+])/);
			if(maskRegExp) {
				mask = maskRegExp[1];
				ip = ip.replace(maskRegExp[1], '');
			}
		}

		const cmd = `arp-scan -qxI ${iface} ${ip}/${mask} | sort -n`;
		const res = await self._execute(cmd, { shell: true });

		if(!res) return self._returns({ error: 'scan failed execution!' });

		const { output, error } = res;

		const data = [];
		for(const line of output.split(/\n|\r/)){
			const split = line.split(/\s/);
			data.push({
					ip: split[0],
					mac: split[1],
				});
		}

		return self._returns({ data });
	}

	async ifaceByIP(ip){
		const self = this;

		const cmd = `ip -o route show to match "${ip}"`;
		const res = await self._execute(cmd);

		if(!res) return self._returns({ error: 'ifaceByIP failed execution!' });

		const { output, error } = res;

		const match = output.split('\n').filter(str => !str.includes('default'));

		if(match.length < 1) {
			return self._returns({
					error: `No Interface detected for this IP Address ${ip}!`,
				});
		}

		const ifRegEx = match[0].match(/dev[\s]{1,}([^\s]+)/);

		if(!ifRegEx){
			return self._returns({
					error: `Can't match any device '${match[0]}'`,
				});
		}

		return self._returns({
				data: { iface: ifRegEx[1].trim() },
			});
	}

	async ping(options = {}){
		const self = this;

		let { ip, iface, timeout, count } = options;

		if(!timeout || isNaN(timeout)) timeout = 20;
		if(!count || isNaN(count)) count = 15;

		if(iface) iface = `-I ${iface}`;

		const cmd = `ping -c${count} -W${timeout} ${iface} ${ip}`;
		const res = await self._execute(cmd, {
				timeout: 0,
				nonBlock: true,
			});

		if(!res) return self._returns({ error: 'ping failed execution!' });

		const { output, error } = res;

		if(error) return self._returns({ error, data: output });

		const matches = {
				loss: output.match(/([0-9]+)\%[\s]+packet[\s]+loss.*?time[\s]+([0-9]+)/),
				result: output.match(/([0-9\.]+)\/([0-9\.]+)\/([0-9\.]+)\/([0-9\.]+)/)
			};

		const data = {
				loss: undefined,
				duration: undefined,
				average: -1,
				min: -1,
				max: -1,
			};

		if(matches.loss){
			data.loss = Number(matches.loss[1]);
			data.duration = Number(matches.loss[2]);
		}

		if(matches.result){
			data.average = Number(matches.result[2]);
			data.min = Number(matches.result[1]);
			data.max = Number(matches.result[3]);
		}

		return self._returns({ data });
	}
}

module.exports = new _ARP_SCAN_HELPER();

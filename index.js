const ArpScan = require('./scan.lib.js');
const Concurrency = require('./concurrence.lib.js');

(async () => {
	const ip = '10.10.0.1';
	const mask = '22';

	const concurrence = new Concurrency({ max: 10, ms: 5 });

	// callback functions
	const ping = (opt) => ArpScan.ping(opt);
	const resolver = async (param) => {
			param.ping = await param.ping.then(res => res.data.average);
		};

	// find network interface to be used for scanning
	const { iface } = await ArpScan.ifaceByIP(ip).then(({ data }) => data);

	// scan availables IP Addresses
	const scan = await ArpScan.scan(iface, ip, mask);

	console.time('ping');

	// get ping for each IP Addresses
	for(const item of scan.data){
		// check maximum concurrent process
		await concurrence.check();

		const params = {
				count: 2,
				ip: item.ip,
				iface,
				timeout: 5,
			};

		// execute ping asynchronously
		item.ping = concurrence.execute(ping, params);

		// resolve promise to get the value
		concurrence.resolve(resolver, item);
	}

	// wait until all promises are resolved
	await concurrence.finish();

	console.timeEnd('ping');

	console.table(scan.data);
})();

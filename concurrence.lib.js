class Concurrency {
	_current = 0;
	_finished = 0;
	_index = 0;
	_waitingList = [];
	_opts = {
			max: 50,
			ms: 5,
		};

	constructor(options){
		const self = this;
		if(options instanceof Object) self.init(options);
	}

	init(options = {}){
		const self = this;

		for(const key in self._opts){
			if(!options[key]) continue;
			self._opts[key] = options[key];
		}

		return self;
	}

	_sleep(ms){
		return new Promise((resolve) => setTimeout(resolve, ms));
	}

	async check(){
		const self = this;

		while(self._current >= self._opts.max) await self._sleep(self._opts.ms);
		return self;
	}

	async execute(fn, ...params){
		const self = this;

		self._current++;

		const index = self._index++;
		self._waitingList[index] = fn(...params);
		await self._waitingList[index];

		self._current--;

		return self._waitingList[index];
	}

	async finish(){
		const self = this;

		const { length } = self._waitingList;
		while(self._finished < length) await self._sleep(self._opts.ms);

		self._waitingList.length = 0;
		self._finished = 0;
		self._index = 0;
	}

	async resolve(cb, ...params){
		await cb(...params);
		this._finished++;
	}

	get length(){
		const self = this;
		return self._current;
	}
}

module.exports = Concurrency;
